# -*- coding: utf-8 -*-
import re
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import tornado.autoreload
from blinker import signal


class ServerError(Exception):
    pass


class InputError(Exception):
    pass


class AuthError(Exception):
    pass


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')


class ChatServer(tornado.web.Application):
    rooms = set()
    clients = set()

    def __init__(self):
        self.subscribe()

        handlers = (
            (r'/ws/?', WebSocketHandler),
            (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': 'static/'}),
            (r'/()$', tornado.web.StaticFileHandler, {'path': 'index.html'})
        )

        tornado.web.Application.__init__(self, handlers, debug=True, autoescape=None)

    def get_client(self, name):
        for client in self.clients:
            if client.name == name:
                return client

        return None

    def get_room(self, roomname):
        for room in self.rooms:
            if room.name == roomname:
                return room

        room = Room(roomname)
        return room

    def broadcast(self, message):
        for client in self.clients:
            client.send_message(message)

    # Server events
    def subscribe(self):
        signal('room:created').connect(self.on_new_room)
        signal('room:deleted').connect(self.on_del_room)
        signal('room:join').connect(self.on_join_room)
        signal('room:leave').connect(self.on_leave_room)
        signal('room:message').connect(self.on_message)
        signal('client:connected').connect(self.on_client_connected)
        signal('client:login').connect(self.on_client_login)
        signal('client:disconnect').connect(self.on_client_disconnect)

    def on_new_room(self, room):
        self.rooms.add(room)
        self.broadcast('NEW ROOM %s' % room)

    def on_del_room(self, room):
        self.rooms.remove(room)
        self.broadcast('DEL ROOM %s' % room)

    def on_join_room(self, room, client):
        room.broadcast('JOIN ROOM %s %s' % (room, client))

    def on_leave_room(self, room, client):
        room.broadcast('LEAVE ROOM %s %s' % (room, client))

    def on_message(self, room, author, text):
        room.broadcast('MESSAGE %s %s %s' % (room, author, text))

    def on_client_login(self, client):
        client.send_message('LOGIN %s' % client)

    def on_client_connected(self, client):
        self.clients.add(client)

    def on_client_disconnect(self, client):
        self.clients.remove(client)


class Room:
    def __init__(self, name):
        self.name = name
        self.__members = set()
        signal('room:created').send(self)
        signal('client:disconnect').connect(self.on_client_disconnect)

    def __repr__(self):
        return self.name

    def add_member(self, client):
        self.__members.add(client)
        signal('room:join').send(self, client=client)

    def remove_member(self, client):
        signal('room:leave').send(self, client=client)
        self.__members.remove(client)

    def broadcast(self, message):
        for client in self.__members:
            client.send_message(message)

    def on_client_disconnect(self, client):
        if client in self.__members:
            self.__members.remove(client)
            signal('room:leave').send(self, client=client)


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    client = None

    def open(self):
        self.client = Client(self)

    def on_message(self, message):
        try:
            print("Got message from client: ", message)
            self.client.handle_message(message)
        except InputError as exc:
            self.client.send_message("ERROR %s" % exc)

    def on_close(self):
        if not self.client:
            return

        self.client.handle_disconnect()
        self.client = None


def login_required(func):
    def wrap(client, *args, **kwargs):
        if not client.name:
            raise AuthError("Not authorized!")

        return func(client, *args, **kwargs)

    return wrap


class Client:
    name = None
    rooms = set()

    def __init__(self, ws):
        self.__write_message = ws.write_message
        self.server = ws.application
        signal('client:connected').send(self)

    def __repr__(self):
        return self.name

    def send_message(self, message):
        print("Send to client:", message)
        return self.__write_message(message)

    def handle_message(self, message):
        if message.startswith('LOGIN NICKNAME'):
            _, _, name = message.split(' ', 2)
            self.handle_login(name)
        elif message.startswith('JOIN ROOM'):
            _, _, roomname = message.split(' ', 2)
            self.handle_join_room(roomname)
        elif message.startswith('LEAVE ROOM'):
            _, _, roomname = message.split(' ', 2)
            self.handle_leave_room(roomname)
        elif message.startswith('MESSAGE'):
            try:
                _, target, text = message.split(' ', 2)
                self.handle_room_message(target, text)
            except Exception:
                raise InputError("Bad arguments!")
        else:
            raise InputError("Unknown command!")

    @property
    def is_logged_in(self):
        return self.name is not None

    def handle_login(self, name):
        if len(name) > 255 or len(name) < 1:
            raise InputError("Nickname length must be in range [1;255]")

        reg = re.compile('^[a-z0-9\.]+$')
        if not reg.match(name):
            raise InputError("Nickname contains not allowed characters")

        client = self.server.get_client(name)
        if client:
            raise InputError("Nickname already in use")

        self.name = name
        signal('client:login').send(self)

    def handle_disconnect(self):
        signal('client:disconnect').send(self)

    @login_required
    def handle_join_room(self, roomname):
        if len(roomname) < 2:
            raise InputError('Roomname should be at least 2 characters long')

        reg = re.compile('^\@[a-z0-9\.]+$')
        if not reg.match(roomname):
            raise InputError("Roomname doesnt match pattern ^\\@[a-z0-9\\.]")

        room = self.server.get_room(roomname)
        room.add_member(self)
        self.rooms.add(room)

    @login_required
    def handle_leave_room(self, roomname):
        room = self.server.get_room(roomname)
        room.remove_member(self)
        self.rooms.remove(room)

    @login_required
    def handle_room_message(self, roomname, text):
        room = self.server.get_room(roomname)
        signal('room:message').send(room, author=self, text=text)


def main():
    app = ChatServer()
    app.listen(8000)
    tornado.autoreload.start()
    tornado.autoreload.watch('index.html')
    tornado.autoreload.watch('app.py')
    tornado.ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()
