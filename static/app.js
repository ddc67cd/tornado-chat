angular.module('chat', [
  'ngWebSocket'
])
.factory('Messages', function($websocket) {
  var ws = $websocket('ws://127.0.0.1:8000/ws/');
  var handler = null;

  ws.onMessage(function(event) {
    var message = event.data;
    console.log('message: ', message);

    if (handler) {
      handler(message);
    };
  });

  ws.onError(function(event) {
    console.log('connection Error', event);
  });

  ws.onClose(function(event) {
    console.log('connection closed', event);
  });

  ws.onOpen(function() {
    console.log('connection open');
  });

  return {
    status: function() {
      return ws.readyState;
    },
    send: function(message) {
      if (angular.isString(message)) {
        ws.send(message);
      }
      else if (angular.isObject(message)) {
        ws.send(JSON.stringify(message));
      }
    },
    callback: function(func) {
      handler = func;
    },
  };
})
.controller('ChatController', function($scope, Messages) {
  $scope.nickname = null;
  $scope.history = {}; // @room -> message list

  $scope.login = function(username) {
    Messages.send('LOGIN NICKNAME ' + username);
  }

  Messages.callback(function(message) {
    if (message.startsWith('LIST')) {
      var args = message.split(' ', 2),
          rooms = args[1];
      $scope.rooms = rooms.split(',');
      if (!$scope.room && $scope.rooms.length) {
        $scope.room = $scope.rooms[0];
      }
      console.log('args = ', args);
    } else if (message.startsWith('LOGIN')) {
      var args = message.split(' ', 2),
          nickname = args[1];
      $scope.nickname = nickname;
      console.log('args = ', args);
    } else if (message.startsWith('MESSAGE')) {
      var args = message.split(' ', 4),
          room = args[1],
          author = args[2],
          text = args[3];
      console.log('args = ', args);

      if (!(room in $scope.history)) {
        $scope.history[room] = [];
      }
      
      $scope.history[room].push({
        text: text,
        author: author
      });

    } else if (message.startsWith('JOIN ROOM')) {
      args = message.split(' ', 4);
      var user = args[3],
          room = args[2];
      if (user == $scope.nickname) {
        if (!(room in $scope.history)) {
          $scope.history[room] = [];
        }
        $scope.room = room;
      }
    } else if (message.startsWith('LEAVE ROOM')) {
      args = message.split(' ', 4);
      var user = args[3],
          room = args[2];

      if (user == $scope.nickname) {
        if (room in $scope.history) {
          delete $scope.history[room];
        }
        $scope.room = null;
      }
    }
  });

  //$scope.Messages.send('JOIN ROOM @someroom2');

  $scope.submit = function(new_message) {
    if (!new_message) { return; }
    Messages.send('MESSAGE ' + $scope.room + ' ' + new_message);
    $scope.new_message = '';
  };

  $scope.join = function(roomName) {
    Messages.send('JOIN ROOM ' + roomName);
    $scope.new_room = '';
  };

  $scope.leave = function(roomName) {
    Messages.send('LEAVE ROOM ' + roomName);
  };

  $scope.rooms = [];
})
.filter('capitalize', function() {
  function capWord(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  }
  return function(input, isEveryWord) {
    return (!input) ? '' : (!isEveryWord) ? capWord(input) : input.replace(/([^\W_]+[^\s-]*) */g, capWord);
  };
});
